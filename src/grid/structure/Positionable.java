package grid.structure;

public interface Positionable {
    public int getX();
    public int getY();
    public Vector getPosition();
}
