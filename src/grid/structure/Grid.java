package grid.structure;

import java.util.ArrayList;

public class Grid implements Sizeable {
    private ArrayList<Piece> pieces;
    private Vector size;

    public synchronized ArrayList<Piece> getPieces() {
        return pieces;
    }

    public Grid(Vector size) {
        pieces = new ArrayList<>();
        this.size = size;
    }

    public synchronized boolean addPiece(Piece piece) {
        if (piece.getX() < 0 || piece.getY() < 0 ||
                piece.getX() + piece.getWidth() > getWidth() ||
                piece.getY() + piece.getHeight() > getHeight())
            return false;
            pieces.add(piece);
        return true;
    }
    
    public synchronized boolean pieceIsColliding(Piece p) {
        if (p.getX() < 0 || p.getY() < 0
                || p.getX() + p.getWidth() > this.getWidth()
                || p.getY() + p.getHeight() > this.getHeight())
            return true;
        for (Piece p2 : pieces)
            if (!p.equals(p2))
                if (p.isCollidingWith(p2))
                    return true;
        return false;
    }
    
    public synchronized void removeEmptyPieces() {
        pieces.removeIf((piece) -> { return piece.getCellCount() == 0; });
    }
    
    @Override
    public synchronized String toString() {
        return pieces.toString();
    }
    
    public synchronized boolean isEmpty(int x, int y) {
        for (Piece piece : pieces)
            if (!piece.isEmptyGlobal(x, y))
                return false;
        return true;
    }
    
    public synchronized boolean lineIsFull(int y) {
        for (int i = 0; i < getWidth(); i++)
            if (isEmpty(i, y))
                return false;
        return true;
    }
    
    public synchronized void removeRow(int y) {
        for (Piece piece : pieces) {
            if (y >= piece.getY() && y < piece.getY() + piece.getHeight())
                piece.removeRow(y - piece.getY());
            if (y >= piece.getY() + piece.getHeight())
                piece.translationForced(new Vector(0, 1));
        }
    }
    
    @Override
    public synchronized int getWidth() {
        return size.getX();
    }

    @Override
    public synchronized int getHeight() {
        return size.getY();
    }

    @Override
    public synchronized Vector getSize() {
        return size;
    }
}
