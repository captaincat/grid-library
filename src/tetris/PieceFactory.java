package tetris;

import grid.structure.Cell;
import grid.structure.Grid;
import grid.structure.Piece;
import grid.structure.Vector;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javafx.scene.paint.Color;

public class PieceFactory {
    private static final Random RANDOM = new Random();
    private static final List<PieceTypes> TYPES =
            Collections.unmodifiableList(Arrays.asList(PieceTypes.values()));
    
    private static Piece buildI(Vector position, Grid grid) {
        Piece p = new Piece(position, new Vector(4, 1), grid);
        p.addCell(new Cell(new Vector(0, 0)));
        p.addCell(new Cell(new Vector(1, 0)));
        p.addCell(new Cell(new Vector(2, 0)));
        p.addCell(new Cell(new Vector(3, 0)));
        return p;
    }
    
    private static Piece buildJ(Vector position, Grid grid) {
        Piece p = new Piece(position, new Vector(3, 2), grid);
        p.addCell(new Cell(new Vector(0, 0)));
        p.addCell(new Cell(new Vector(1, 0)));
        p.addCell(new Cell(new Vector(2, 0)));
        p.addCell(new Cell(new Vector(2, 1)));
        return p;
    }
    
    private static Piece buildL(Vector position, Grid grid) {
        Piece p = new Piece(position, new Vector(3, 2), grid);
        p.addCell(new Cell(new Vector(0, 0)));
        p.addCell(new Cell(new Vector(1, 0)));
        p.addCell(new Cell(new Vector(2, 0)));
        p.addCell(new Cell(new Vector(0, 1)));
        return p;
    }
    
    private static Piece buildO(Vector position, Grid grid) {
        Piece p = new Piece(position, new Vector(2, 2), grid);
        p.addCell(new Cell(new Vector(0, 0)));
        p.addCell(new Cell(new Vector(1, 0)));
        p.addCell(new Cell(new Vector(0, 1)));
        p.addCell(new Cell(new Vector(1, 1)));
        return p;
    }
    
    private static Piece buildS(Vector position, Grid grid) {
        Piece p = new Piece(position, new Vector(3, 2), grid);
        p.addCell(new Cell(new Vector(0, 1)));
        p.addCell(new Cell(new Vector(1, 1)));
        p.addCell(new Cell(new Vector(1, 0)));
        p.addCell(new Cell(new Vector(2, 0)));
        return p;
    }
    
    private static Piece buildT(Vector position, Grid grid) {
        Piece p = new Piece(position, new Vector(3, 2), grid);
        p.addCell(new Cell(new Vector(0, 0)));
        p.addCell(new Cell(new Vector(1, 0)));
        p.addCell(new Cell(new Vector(2, 0)));
        p.addCell(new Cell(new Vector(1, 1)));
        return p;
    }
    
    private static Piece buildZ(Vector position, Grid grid) {
        Piece p = new Piece(position, new Vector(3, 2), grid);
        p.addCell(new Cell(new Vector(0, 0)));
        p.addCell(new Cell(new Vector(1, 0)));
        p.addCell(new Cell(new Vector(1, 1)));
        p.addCell(new Cell(new Vector(2, 1)));
        return p;
    }
    
    public static Color getColor(PieceTypes type) {
        switch (type) {
            case I:
                return Color.CYAN;
            case J:
                return Color.BLUE;
            case L:
                return Color.ORANGE;
            case O:
                return Color.YELLOW;
            case S:
                return Color.LIME;
            case T:
                return Color.PURPLE;
            case Z:
                return Color.RED;
            default:
                return null;
        }
    }
    
    public static Piece buildPiece(Vector position, PieceTypes type, Grid grid) {
        switch (type) {
            case I:
                return buildI(position, grid);
            case J:
                return buildJ(position, grid);
            case L:
                return buildL(position, grid);
            case O:
                return buildO(position, grid);
            case S:
                return buildS(position, grid);
            case T:
                return buildT(position, grid);
            case Z:
                return buildZ(position, grid);
            default:
                return null;
        }
    }
    
    public static PieceTypes pickRandomType() {
        return TYPES.get(RANDOM.nextInt(TYPES.size()));
    }
}
